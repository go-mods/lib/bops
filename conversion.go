package bops

func ToInt(a []byte) int {
	n := 0
	l := len(a)

	switch {
	case l > 3:
		n |= int(a[3]) << 24
		fallthrough
	case l > 2:
		n |= int(a[2]) << 16
		fallthrough
	case l > 1:
		n |= int(a[1]) << 8
		fallthrough
	case l > 0:
		n |= int(a[0])
	}

	return n
}

func FromInt(n int) []byte {
	return []byte{
		byte(n >> 24),
		byte(n >> 16),
		byte(n >> 8),
		byte(n),
	}
}
