package bops

import "testing"

func TestStr(t *testing.T) {
	type args struct {
		a []byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "str(0xFC43)",
			args: args{
				a: []byte{0xFC, 0x43},
			},
			want: "1111110001000011",
		},
		{
			name: "str(0x00FF)",
			args: args{
				a: []byte{0x00, 0xFF},
			},
			want: "0000000011111111",
		},
		{
			name: "str(0x00)",
			args: args{
				a: []byte{0x00},
			},
			want: "00000000",
		},
		{
			name: "str(0x010001)",
			args: args{
				a: []byte{0x01, 0x00, 0x01},
			},
			want: "000000010000000000000001",
		},
		{
			name: "str()",
			args: args{
				a: []byte{},
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Str(tt.args.a); got != tt.want {
				t.Errorf("Str() = %v, want %v", got, tt.want)
			}
		})
	}
}
