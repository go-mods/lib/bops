// Bops provides functions for binary (bitwise) operations on
// sequences of bytes with arbitrary length.
package bops
