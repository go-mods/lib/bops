# bops

Bops provides functions for binary (bitwise) operations on
sequences of bytes with arbitrary length.

## Functions

### func [Add](/arithmetic.go#L49)

`func Add(a, b []byte) []byte`

Add takes two slices of bytes in big-endian representation and
adds b to a. The result is returned as a new slice of bytes with
the same length as the longer of the given slices.

### func [And](/bitwise.go#L21)

`func And(a, b []byte) []byte`

And takes two slices of bytes in big-endian representation and
performs an and-operation for each pair of bytes. The result is
returned as a new slice of bytes with the same length as the
longer of the given slices.

### func [Div](/arithmetic.go#L106)

`func Div(a, b []byte) []byte`

Div takes two slices of bytes in big-endian representation and
divides a by b. The result is returned as a new slice of bytes
with the same length as the longer of the given slices.

### func [Mul](/arithmetic.go#L86)

`func Mul(a, b []byte) []byte`

Mul takes two slices of bytes in big-endian representation and
multiplies a with b. The result is returned as a new slice of
bytes with the same length as the longer of the given slices.

### func [Nand](/bitwise.go#L38)

`func Nand(a, b []byte) []byte`

Nand takes two slices of bytes in big-endian representation and
performs a nand-operation for each pair of bytes. The result is
returned as a new slice of bytes with the same length as the
longer of the given slices.

### func [Nor](/bitwise.go#L72)

`func Nor(a, b []byte) []byte`

Nor takes two slices of bytes in big-endian representation and
performs a nor-operation for each pair of bytes. The result is
returned as a new slice of bytes with the same length as the
longer of the given slices.

### func [Not](/bitwise.go#L6)

`func Not(a []byte) []byte`

Not takes a slice of bytes in big-endian representation and
performs a not-operation for each byte. The result is returned as
a new slice of bytes with the same length.

### func [Or](/bitwise.go#L55)

`func Or(a, b []byte) []byte`

Or takes two slices of bytes in big-endian representation and
performs an or-operation for each pair of bytes. The result is
returned as a new slice of bytes with the same length as the
longer of the given slices.

### func [ShiftL](/arithmetic.go#L13)

`func ShiftL(a []byte, n int) []byte`

ShiftL takes a slice of bytes in big-endian representation and
shifts the bits of each byte n positions left. Bits that would be
discarded this way will be added to the previous byte
accordingly. Returns the result as a new slice of bytes with the
same length as a.

### func [ShiftR](/arithmetic.go#L32)

`func ShiftR(a []byte, n int) []byte`

ShiftR takes a slice of bytes in big-endian representation and
shifts the bits of each byte n positions right. Bits that would
be discarded this way will be added to the next byte accordingly.
Returns the result as a new slice of bytes with the same length
as a.

### func [Str](/misc.go#L7)

`func Str(a []byte) string`

Str takes a slice of bytes in big-endian representation and
returns a binary representation of the value as string.

### func [Sub](/arithmetic.go#L68)

`func Sub(a, b []byte) []byte`

Sub takes two slices of bytes in big-endian representation and
subtracts b from a. The result is returned as a new slice of
bytes with the same length as the longer of the given slices.

### func [Xor](/bitwise.go#L89)

`func Xor(a, b []byte) []byte`

Xor takes two slices of bytes in big-endian representation and
performs an xor-operation for each pair of bytes. The result is
returned as a new slice of bytes with the same length as the
longer of the given slices.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
