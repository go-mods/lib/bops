package bops

import (
	"math"
	"reflect"
	"testing"
)

func TestShiftL(t *testing.T) {
	type args struct {
		a []byte
		n int
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x0180<<1",
			args: args{
				a: []byte{0x01, 0x80},
				n: 1,
			},
			want: []byte{0x03, 0x00},
		},
		{
			name: "0x0001<<1",
			args: args{
				a: []byte{0x00, 0x01},
				n: 1,
			},
			want: []byte{0x00, 0x02},
		},
		{
			name: "0x004180<<9",
			args: args{
				a: []byte{0x00, 0x41, 0x80},
				n: 9,
			},
			want: []byte{0x83, 0x00, 0x00},
		},
		{
			name: "0x004180<<16",
			args: args{
				a: []byte{0x00, 0x41, 0x80},
				n: 16,
			},
			want: []byte{0x80, 0x00, 0x00},
		},
		{
			name: "0x004180<<17",
			args: args{
				a: []byte{0x00, 0x41, 0x80},
				n: 17,
			},
			want: []byte{0x00, 0x00, 0x00},
		},
		{
			name: "<<42",
			args: args{
				a: []byte{},
				n: 42,
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ShiftL(tt.args.a, tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ShiftL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestShiftR(t *testing.T) {
	type args struct {
		a []byte
		n int
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x0180>>1",
			args: args{
				a: []byte{0x01, 0x80},
				n: 1,
			},
			want: []byte{0x00, 0xC0},
		},
		{
			name: "0x0001>>1",
			args: args{
				a: []byte{0x00, 0x01},
				n: 1,
			},
			want: []byte{0x00, 0x00},
		},
		{
			name: "0x014100>>9",
			args: args{
				a: []byte{0x01, 0x41, 00},
				n: 9,
			},
			want: []byte{0x00, 0x00, 0xA0},
		},
		{
			name: "0x014100>>16",
			args: args{
				a: []byte{0x01, 0x41, 0x00},
				n: 16,
			},
			want: []byte{0x00, 0x00, 0x01},
		},
		{
			name: "0x014100>>17",
			args: args{
				a: []byte{0x01, 0x41, 0x00},
				n: 17,
			},
			want: []byte{0x00, 0x00, 0x00},
		},
		{
			name: ">>42",
			args: args{
				a: []byte{},
				n: 17,
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ShiftR(tt.args.a, tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ShiftR() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdd(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x34+0x6B",
			args: args{
				a: []byte{0x34},
				b: []byte{0x6B},
			},
			want: []byte{0x9F},
		},
		{
			name: "0xAE+0xC8",
			args: args{
				a: []byte{0xAE},
				b: []byte{0xC8},
			},
			want: []byte{0x76},
		},
		{
			name: "0x0001+0x01",
			args: args{
				a: []byte{0x00, 0x01},
				b: []byte{0x01},
			},
			want: []byte{0x00, 0x02},
		},
		{
			name: "0xFFFF+0xEA",
			args: args{
				a: []byte{0xFF, 0xFF},
				b: []byte{0xEA},
			},
			want: []byte{0x00, 0xE9},
		},
		{
			name: "0x36+0x38A1",
			args: args{
				a: []byte{0x36},
				b: []byte{0x38, 0xA1},
			},
			want: []byte{0x38, 0xD7},
		},
		{
			name: "0x01+0x0001",
			args: args{
				a: []byte{0x01},
				b: []byte{0x00, 0x01},
			},
			want: []byte{0x00, 0x02},
		},
		{
			name: "0x5A16+",
			args: args{
				a: []byte{0x5A, 0x16},
				b: []byte{},
			},
			want: []byte{0x5A, 0x16},
		},
		{
			name: "+0xF2",
			args: args{
				a: []byte{0xF2},
				b: []byte{},
			},
			want: []byte{0xF2},
		},
		{
			name: "+",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Add(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSub(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x51-0x3E",
			args: args{
				a: []byte{0x51},
				b: []byte{0x3E},
			},
			want: []byte{0x13},
		},
		{
			name: "0xCA34-0x8B",
			args: args{
				a: []byte{0xCA, 0x34},
				b: []byte{0x8B},
			},
			want: []byte{0xC9, 0xA9},
		},
		{
			name: "0x0100-0x01",
			args: args{
				a: []byte{0x01, 0x00},
				b: []byte{0x01},
			},
			want: []byte{0x00, 0xFF},
		},
		{
			name: "0x41-0x3A1A",
			args: args{
				a: []byte{0x41},
				b: []byte{0x3A, 0x1A},
			},
			want: []byte{0x39, 0xD9},
		},
		{
			name: "0x6C-",
			args: args{
				a: []byte{0x6C},
				b: []byte{},
			},
			want: []byte{0x6C},
		},
		{
			name: "-0x4E",
			args: args{
				a: []byte{},
				b: []byte{0x4E},
			},
			want: []byte{0x4E},
		},
		{
			name: "-",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Sub(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Sub() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMul(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0xE1*0x14",
			args: args{
				a: []byte{0xE1},
				b: []byte{0x14},
			},
			want: []byte{0x94},
		},
		{
			name: "0x24A1*0x03",
			args: args{
				a: []byte{0x24, 0xA1},
				b: []byte{0x03},
			},
			want: []byte{0x6D, 0xE3},
		},
		{
			name: "0x0001*0xAB",
			args: args{
				a: []byte{0x00, 0x01},
				b: []byte{0xAB},
			},
			want: []byte{0x00, 0xAB},
		},
		{
			name: "0x81*0x020A",
			args: args{
				a: []byte{0x81},
				b: []byte{0x02, 0x0A},
			},
			want: []byte{0x07, 0x0A},
		},
		{
			name: "0x25*",
			args: args{
				a: []byte{0x25},
				b: []byte{},
			},
			want: []byte{0x00},
		},
		{
			name: "*0xE3",
			args: args{
				a: []byte{},
				b: []byte{0xE3},
			},
			want: []byte{0x00},
		},
		{
			name: "*",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Mul(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Mul() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDiv(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x54/0x13",
			args: args{
				a: []byte{0x54},
				b: []byte{0x13},
			},
			want: []byte{0x04},
		},
		{
			name: "0xA341/0xB6",
			args: args{
				a: []byte{0xA3, 0x41},
				b: []byte{0xB6},
			},
			want: []byte{0x00, 0xE5},
		},
		{
			name: "0x0100/0x02",
			args: args{
				a: []byte{0x01, 0x00},
				b: []byte{0x02},
			},
			want: []byte{0x00, 0x80},
		},
		{
			name: "0x52/0x0F14",
			args: args{
				a: []byte{0x52},
				b: []byte{0x0F, 0x14},
			},
			want: []byte{0x00, 0x00},
		},
		{
			name: "/0x05",
			args: args{
				a: []byte{},
				b: []byte{0x05},
			},
			want: []byte{0x00},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Div(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Div() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_max(t *testing.T) {
	type args struct {
		n []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "max(1,3,1)",
			args: args{
				n: []int{1, 3, 1},
			},
			want: 3,
		},
		{
			name: "max(1,-3,1)",
			args: args{
				n: []int{1, -3, 1},
			},
			want: 1,
		},
		{
			name: "max(1,2)",
			args: args{
				n: []int{1, 2},
			},
			want: 2,
		},
		{
			name: "max()",
			args: args{
				[]int{},
			},
			want: math.MinInt,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := max(tt.args.n...); got != tt.want {
				t.Errorf("max() = %v, want %v", got, tt.want)
			}
		})
	}
}
