package bops

import (
	"reflect"
	"testing"
)

func TestNot(t *testing.T) {
	type args struct {
		a []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "^0x00",
			args: args{
				a: []byte{0x00},
			},
			want: []byte{0xFF},
		},
		{
			name: "^0xFF",
			args: args{
				a: []byte{0xFF},
			},
			want: []byte{0x00},
		},
		{
			name: "^0x0FF0",
			args: args{
				a: []byte{0x0F, 0xF0},
			},
			want: []byte{0xF0, 0x0F},
		},
		{
			name: "^",
			args: args{
				a: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Not(tt.args.a); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Not() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAnd(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x3A&0xB3",
			args: args{
				a: []byte{0x3A},
				b: []byte{0xB3},
			},
			want: []byte{0x32},
		},
		{
			name: "0x0101&0x01",
			args: args{
				a: []byte{0x01, 0x01},
				b: []byte{0x01},
			},
			want: []byte{0x00, 0x01},
		},
		{
			name: "0x72&0xAF13",
			args: args{
				a: []byte{0x72},
				b: []byte{0xAF, 0x13},
			},
			want: []byte{0x00, 0x12},
		},
		{
			name: "0x6AC0&0x4F",
			args: args{
				a: []byte{0x6A, 0xC0},
				b: []byte{0x4F},
			},
			want: []byte{0x00, 0x40},
		},
		{
			name: "0x12&",
			args: args{
				a: []byte{0x12},
				b: []byte{},
			},
			want: []byte{0x00},
		},
		{
			name: "&",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := And(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("And() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNand(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "^(0x42&0xAB)",
			args: args{
				a: []byte{0x42},
				b: []byte{0xAB},
			},
			want: []byte{0xFD},
		},
		{
			name: "^(0x1AE3&0x4E)",
			args: args{
				a: []byte{0x1A, 0xE3},
				b: []byte{0x4E},
			},
			want: []byte{0xFF, 0xBD},
		},
		{
			name: "^(0x0101&0x01)",
			args: args{
				a: []byte{0x01, 0x01},
				b: []byte{0x01},
			},
			want: []byte{0xFF, 0xFE},
		},
		{
			name: "^(0x73&0x821F)",
			args: args{
				a: []byte{0x73},
				b: []byte{0x82, 0x1F},
			},
			want: []byte{0xFF, 0xEC},
		},
		{
			name: "^(0xEE&)",
			args: args{
				a: []byte{0xEE},
				b: []byte{},
			},
			want: []byte{0xFF},
		},
		{
			name: "^(&0xAE1B)",
			args: args{
				a: []byte{},
				b: []byte{0xAE, 0x1B},
			},
			want: []byte{0xFF, 0xFF},
		},
		{
			name: "^(&)",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Nand(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Nand() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOr(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x2E|0xC1",
			args: args{
				a: []byte{0x2E},
				b: []byte{0xC1},
			},
			want: []byte{0xEF},
		},
		{
			name: "0xA43F|0x16",
			args: args{
				a: []byte{0xA3, 0x4F},
				b: []byte{0x16},
			},
			want: []byte{0xA3, 0x5F},
		},
		{
			name: "0x0001|0x02",
			args: args{
				a: []byte{0x00, 0x01},
				b: []byte{0x02},
			},
			want: []byte{0x00, 0x03},
		},
		{
			name: "0xE3|0x2BBC",
			args: args{
				a: []byte{0xE3},
				b: []byte{0x2B, 0xBC},
			},
			want: []byte{0x2B, 0xFF},
		},
		{
			name: "0x3E2A|",
			args: args{
				a: []byte{0x3E, 0x2A},
				b: []byte{},
			},
			want: []byte{0x3E, 0x2A},
		},
		{
			name: "|0x07",
			args: args{
				a: []byte{},
				b: []byte{0x07},
			},
			want: []byte{0x07},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Or(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Or() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNor(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "^(0x13|0x4A)",
			args: args{
				a: []byte{0x13},
				b: []byte{0x4A},
			},
			want: []byte{0xA4},
		},
		{
			name: "^(0xA724|0xC1)",
			args: args{
				a: []byte{0xA7, 0x24},
				b: []byte{0xC1},
			},
			want: []byte{0x58, 0x1A},
		},
		{
			name: "^(0x0001|0x02)",
			args: args{
				a: []byte{0x00, 0x01},
				b: []byte{0x02},
			},
			want: []byte{0xFF, 0xFC},
		},
		{
			name: "^(0x63|0x284E)",
			args: args{
				a: []byte{0x63},
				b: []byte{0x28, 0x4E},
			},
			want: []byte{0xD7, 0x90},
		},
		{
			name: "^(0xE21F|)",
			args: args{
				a: []byte{0xE2, 0x1F},
				b: []byte{},
			},
			want: []byte{0x1D, 0xE0},
		},
		{
			name: "^(|0xA3)",
			args: args{
				a: []byte{},
				b: []byte{0xA3},
			},
			want: []byte{0x5C},
		},
		{
			name: "^(|)",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Nor(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Nor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestXor(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "0x0A01^0x10F0",
			args: args{
				a: []byte{0x0A, 0x01},
				b: []byte{0x10, 0xF0},
			},
			want: []byte{0x1A, 0xF1},
		},
		{
			name: "0x0A01FA^0x10F0",
			args: args{
				a: []byte{0x0A, 0x01, 0xFA},
				b: []byte{0x10, 0xF0},
			},
			want: []byte{0x0A, 0x11, 0x0A},
		},
		{
			name: "0x0003^0x02",
			args: args{
				a: []byte{0x00, 0x03},
				b: []byte{0x02},
			},
			want: []byte{0x00, 0x01},
		},
		{
			name: "0x0A01^0xC710F0",
			args: args{
				a: []byte{0x0A, 0x01},
				b: []byte{0xC7, 0x10, 0xF0},
			},
			want: []byte{0xC7, 0x1A, 0xF1},
		},
		{
			name: "0x5A^",
			args: args{
				a: []byte{0x5A},
				b: []byte{},
			},
			want: []byte{0x5A},
		},
		{
			name: "^0x78AF",
			args: args{
				a: []byte{},
				b: []byte{0x78, 0xAF},
			},
			want: []byte{0x78, 0xAF},
		},
		{
			name: "^",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want: []byte{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Xor(tt.args.a, tt.args.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Xor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_prep(t *testing.T) {
	type args struct {
		a []byte
		b []byte
	}
	tests := []struct {
		name  string
		args  args
		want  []byte
		want1 []byte
		want2 []byte
		want3 int
	}{
		{
			name: "prep(0x4C,0xAB)",
			args: args{
				a: []byte{0x4C},
				b: []byte{0xAB},
			},
			want:  []byte{0x4C},
			want1: []byte{0xAB},
			want2: []byte{0x00},
			want3: 1,
		},
		{
			name: "prep(0x4C,0xD1AB)",
			args: args{
				a: []byte{0x4C},
				b: []byte{0xD1, 0xAB},
			},
			want:  []byte{0x00, 0x4C},
			want1: []byte{0xD1, 0xAB},
			want2: []byte{0x00, 0x00},
			want3: 2,
		},
		{
			name: "prep(,0xD1AB)",
			args: args{
				a: []byte{},
				b: []byte{0xD1, 0xAB},
			},
			want:  []byte{0x00, 0x00},
			want1: []byte{0xD1, 0xAB},
			want2: []byte{0x00, 0x00},
			want3: 2,
		},
		{
			name: "prep(0xD431,)",
			args: args{
				a: []byte{0xD4, 0x31},
				b: []byte{},
			},
			want:  []byte{0xD4, 0x31},
			want1: []byte{0x00, 0x00},
			want2: []byte{0x00, 0x00},
			want3: 2,
		},
		{
			name: "prep(,)",
			args: args{
				a: []byte{},
				b: []byte{},
			},
			want:  []byte{},
			want1: []byte{},
			want2: []byte{},
			want3: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2, got3 := prep(tt.args.a, tt.args.b)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("prep() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("prep() got1 = %v, want %v", got1, tt.want1)
			}
			if !reflect.DeepEqual(got2, tt.want2) {
				t.Errorf("prep() got2 = %v, want %v", got2, tt.want2)
			}
			if got3 != tt.want3 {
				t.Errorf("prep() got3 = %v, want %v", got3, tt.want3)
			}
		})
	}
}
