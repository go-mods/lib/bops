package bops

import (
	"reflect"
	"testing"
)

func TestToInt(t *testing.T) {
	type args struct {
		a []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "ToInt(0x0101)",
			args: args{
				a: []byte{0x01, 0x01},
			},
			want: 257,
		},
		{
			name: "ToInt(0x0100000001)",
			args: args{
				a: []byte{0x01, 0x00, 0x00, 0x00, 0x01},
			},
			want: 1,
		},
		{
			name: "ToInt(0xFF)",
			args: args{
				a: []byte{0xFF},
			},
			want: 255,
		},
		{
			name: "ToInt()",
			args: args{
				a: []byte{},
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToInt(tt.args.a); got != tt.want {
				t.Errorf("ToInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromInt(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "FromInt(256)",
			args: args{n: 256},
			want: []byte{0x00, 0x00, 0x01, 0x00},
		},
		{
			name: "FromInt(255)",
			args: args{n: 255},
			want: []byte{0x00, 0x00, 0x00, 0xFF},
		},
		{
			name: "FromInt(0)",
			args: args{n: 0},
			want: []byte{0x00, 0x00, 0x00, 0x00},
		},
		{
			name: "FromInt(-1)",
			args: args{n: -2},
			want: []byte{0xFF, 0xFF, 0xFF, 0xFE},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromInt(tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
